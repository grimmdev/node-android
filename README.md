1. Year: 2016
2. Author: Sean Loper
3. Requirements: Armv7, Root/SuperUser, BusyBox
4. Description: This is the binary repository for Node.js for Android. Doesn't support NPM atm and only Armv7.
5. Steps: After you download the repository, use the commands in the txt file according to your device.
6. Note: These are compiled NodeJS Binaries from 6.0.0, these steps where gathered from numerous sources on the internet. This just saves you most of the heavy work.